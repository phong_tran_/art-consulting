<?php
global $avia_config;

	/*
	 * get_header is a basic wordpress function, used to retrieve the header.php file in your theme directory.
	 */
	 get_header();


 	 if( get_post_meta(get_the_ID(), 'header', true) != 'no') echo avia_title();
	 ?>

		<div class='container_wrap container_wrap_first main_color <?php avia_layout_class( 'main' ); ?>'>

			<div class='container'>

				<main class=' template-blog template-single-blog template-activity content  <?php avia_layout_class( 'content' ); ?> units' <?php avia_markup_helper(array('context' => 'content','post_type'=>'activity'));?>>

                    <div style="padding-bottom:0px;" class="av-special-heading av-special-heading-h3    avia-builder-el-1  el_before_av_textblock  avia-builder-el-first  style1">
                        <h3 class="av-special-heading-tag" itemprop="headline">Our Staff</h3>
                        <div class="special-heading-border">
                            <div class="special-heading-inner-border"></div>
                        </div>
                    </div>
                    <?php
                    /* Run the loop to output the posts.
                    * If you want to overload this in a child theme then include a file
                    * called loop-page.php and that will be used instead.
                    */

                    get_template_part( 'includes/loop', 'staff' );

                    ?>

                <!--end content-->

				</main>

				<?php

				//get the sidebar
				$avia_config['currently_viewing'] = 'page';
				get_sidebar();

				?>

			</div><!--end container-->

		</div><!-- close default .container_wrap element -->



<?php get_footer(); ?>