<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 
// Add "custom CSS" field for Enfold builder elements
add_theme_support('avia_template_builder_custom_css');
function modify_jquery() {
    if (!is_admin()) {
        wp_deregister_script('jquery');
        wp_register_script('jquery', 'https://code.jquery.com/jquery-1.11.3.min.js');
        wp_enqueue_script('jquery');
    }
}
add_action('init', 'modify_jquery');
function wpdocs_theme_name_scripts() {
    wp_enqueue_style( 'style-name', get_stylesheet_uri() );
    wp_enqueue_script( 'script-name', get_stylesheet_directory_uri() . '/script.js', array(), '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'wpdocs_theme_name_scripts' );



if(!function_exists('avia_custom_query_extension'))
{
    function avia_custom_query_extension($query, $params)
    {
        global $avia_config;
        if(!empty($avia_config['avia_custom_query_options']['order']))
        {
            $query['order'] = $avia_config['avia_custom_query_options']['order'];
        }

        if(!empty($avia_config['avia_custom_query_options']['orderby']))
        {
            $query['orderby'] = $avia_config['avia_custom_query_options']['orderby'];
        }

        unset($avia_config['avia_custom_query_options']);

        return $query;
    }

    add_filter('avia_masonry_entries_query', 'avia_custom_query_extension', 10, 2);
    add_filter('avia_post_grid_query', 'avia_custom_query_extension', 10, 2);
    add_filter('avia_post_slide_query', 'avia_custom_query_extension', 10, 2);
    add_filter('avia_blog_post_query', 'avia_custom_query_extension', 10, 2);
    add_filter('avf_magazine_entries_query', 'avia_custom_query_extension', 10, 2);

    add_filter('avf_template_builder_shortcode_elements','avia_custom_query_options', 10, 1);
    function avia_custom_query_options($elements)
    {
        $allowed_elements = array('av_blog','av_masonry_entries','av_postslider','av_portfolio','av_magazine');

        if(isset($_POST['params']['allowed']) && in_array($_POST['params']['allowed'], $allowed_elements))
        {
            $elements[] = array(
                "name" => __("Custom Query Orderby",'avia_framework' ),
                "desc" => __("Set a custom query orderby value",'avia_framework' ),
                "id"   => "orderby",
                "type" 	=> "select",
                "std" 	=> "",
                "subtype" => array(
                    __('Default Order',  'avia_framework' ) =>'',
                    __('Title',  'avia_framework' ) =>'title',
                    __('Random',  'avia_framework' ) =>'rand',
                    __('Date',  'avia_framework' ) =>'date',
                    __('Author',  'avia_framework' ) =>'author',
                    __('Name (Post Slug)',  'avia_framework' ) =>'name',
                    __('Modified',  'avia_framework' ) =>'modified',
                    __('Comment Count',  'avia_framework' ) =>'comment_count',
                    __('Page Order',  'avia_framework' ) =>'menu_order')
            );

            $elements[] = array(
                "name" => __("Custom Query Order",'avia_framework' ),
                "desc" => __("Set a custom query order",'avia_framework' ),
                "id"   => "order",
                "type" 	=> "select",
                "std" 	=> "",
                "subtype" => array(
                    __('Default Order',  'avia_framework' ) =>'',
                    __('Ascending Order',  'avia_framework' ) =>'ASC',
                    __('Descending Order',  'avia_framework' ) =>'DESC'));
        }

        return $elements;
    }

    add_filter('avf_template_builder_shortcode_meta', 'avia_custom_query_add_query_params_to_config', 10, 4);
    function avia_custom_query_add_query_params_to_config($meta, $atts, $content, $shortcodename)
    {
        global $avia_config;
        if(empty($avia_config['avia_custom_query_options'])) $avia_config['avia_custom_query_options'] = array();

        if(!empty($atts['order']))
        {
            $avia_config['avia_custom_query_options']['order'] = $atts['order'];
        }

        if(!empty($atts['orderby']))
        {
            $avia_config['avia_custom_query_options']['orderby'] = $atts['orderby'];
        }

        return $meta;
    }
}



function vcomment_list_func( $atts ) {
    $a = shortcode_atts( array(
        'id' => '1',
    ), $atts );
    $args = array(
        'posts_per_page'   => -1,
        'offset'           => 0,
        'category'         => $a['id'],
        'category_name'    => '',
        'orderby'          => 'date',
        'order'            => 'DESC',
        'include'          => '',
        'exclude'          => '',
        'meta_key'         => '',
        'meta_value'       => '',
        'post_type'        => 'post',
        'post_mime_type'   => '',
        'post_parent'      => '',
        'author'	   => '',
        'author_name'	   => '',
        'post_status'      => 'publish',
        'suppress_filters' => true
    );
    $posts_array = get_posts( $args );
    $html = '<div class="v_list">';
    foreach($posts_array as $post){
        $thum_image = wp_get_attachment_image_src ( get_post_thumbnail_id($post->ID),'thumbnail',false );
        $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
        $html .='<div class="item">
                    <div class="fea-img img'.$post->ID.'">
                        <a href="#content'.$post->ID.'" ><img src="'.$thum_image[0].'" alt="" /></a>
                    </div>
                    <div id="content'.$post->ID.'" class="white-popup mfp-hide"><div><img src="'.$feat_image.'" alt="" /></div><div>'.apply_filters('the_content', $post->post_content).'</div></div>
                 </div>';
    }
    $html .='</div>';
    return $html;
}
add_shortcode( 'v_lists', 'vcomment_list_func' );





/*
* Creating a function to create our CPT
*/

function custom_post_type() {

// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Activity', 'Post Type General Name', 'twentythirteen' ),
        'singular_name'       => _x( 'Activity', 'Post Type Singular Name', 'twentythirteen' ),
        'menu_name'           => __( 'Activity', 'twentythirteen' ),
        'parent_item_colon'   => __( 'Parent Activity', 'twentythirteen' ),
        'all_items'           => __( 'All activity', 'twentythirteen' ),
        'view_item'           => __( 'View Activity', 'twentythirteen' ),
        'add_new_item'        => __( 'Add New Activity', 'twentythirteen' ),
        'add_new'             => __( 'Add New', 'twentythirteen' ),
        'edit_item'           => __( 'Edit Activity', 'twentythirteen' ),
        'update_item'         => __( 'Update Activity', 'twentythirteen' ),
        'search_items'        => __( 'Search Activity', 'twentythirteen' ),
        'not_found'           => __( 'Not Found', 'twentythirteen' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'twentythirteen' ),
    );

// Set UI labels for Custom Post Type
    $labels_staff = array(
        'name'                => _x( 'Staff', 'Post Type General Name', 'twentythirteen' ),
        'singular_name'       => _x( 'Staff', 'Post Type Singular Name', 'twentythirteen' ),
        'menu_name'           => __( 'Staff', 'twentythirteen' ),
        'parent_item_colon'   => __( 'Parent Staff', 'twentythirteen' ),
        'all_items'           => __( 'All Staff', 'twentythirteen' ),
        'view_item'           => __( 'View Staff', 'twentythirteen' ),
        'add_new_item'        => __( 'Add New Staff', 'twentythirteen' ),
        'add_new'             => __( 'Add New', 'twentythirteen' ),
        'edit_item'           => __( 'Edit Staff', 'twentythirteen' ),
        'update_item'         => __( 'Update Staff', 'twentythirteen' ),
        'search_items'        => __( 'Search Staff', 'twentythirteen' ),
        'not_found'           => __( 'Not Found', 'twentythirteen' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'twentythirteen' ),
    );


// Set other options for Custom Post Type

    $args = array(
        'label'               => __( 'Activity', 'twentythirteen' ),
        'description'         => __( 'Description', 'twentythirteen' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor',   'thumbnail',  'revisions' ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */
        'rewrite' => array('slug' => 'activity'),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
        'taxonomies'          => array( 'activity-type' ),
    );

    // Set other options for Custom Post Type

    $args_staff = array(
        'label'               => __( 'Staff', 'twentythirteen' ),
        'description'         => __( 'Description', 'twentythirteen' ),
        'labels'              => $labels_staff,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor',   'thumbnail',  'revisions','excerpt' ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */
        'rewrite' => array('slug' => 'staff'),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => false,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
        'taxonomies'          => array( 'staff-type' ),
    );

    // Registering your Custom Post Type
    register_post_type( 'activity', $args );
    register_post_type( 'staff', $args_staff );

}

/* Hook into the 'init' action so that the function
* Containing our post type registration is not
* unnecessarily executed.
*/

add_action( 'init', 'custom_post_type', 0 );

add_filter('pre_get_posts', 'query_post_type');
function query_post_type($query) {
    if( is_category() ) {
        $post_type = get_query_var('post_type');
        if($post_type)
            $post_type = $post_type;
        else
            $post_type = array('nav_menu_item', 'post', 'activity'); // don't forget nav_menu_item to allow menus to work!
        $query->set('post_type',$post_type);
        return $query;
    }
}

add_action( 'init', 'wpsites_custom_taxonomy_types' );
function wpsites_custom_taxonomy_types() {

    register_taxonomy( 'activity-type', 'activity',
        array(
            'labels' => array(
                'name'          => _x( 'Category', 'taxonomy general name', 'twentythirteen' ),
                'add_new_item'  => __( 'Add New Activity Category', 'twentythirteen' ),
                'new_item_name' => __( 'New Activity Category', 'twentythirteen' ),
            ),
            'exclude_from_search' => true,
            'has_archive'         => true,
            'hierarchical'        => true,
            'rewrite'             => array( 'slug' => 'activity-type', 'with_front' => false ),
            'show_ui'             => true,
            'show_tagcloud'       => false,
        ));
    register_taxonomy( 'staff-type', 'staff',
        array(
            'labels' => array(
                'name'          => _x( 'Category', 'taxonomy general name', 'twentythirteen' ),
                'add_new_item'  => __( 'Add New Staff Category', 'twentythirteen' ),
                'new_item_name' => __( 'New Staff Category', 'twentythirteen' ),
            ),
            'exclude_from_search' => true,
            'has_archive'         => false,
            'hierarchical'        => true,
            'rewrite'             => array( 'slug' => 'staff-type', 'with_front' => false ),
            'show_ui'             => true,
            'show_tagcloud'       => false,
        ));

}


?>

