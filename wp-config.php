<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_art');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '?Z.33]YaxZ]?W0WyuZF%4<%.x7%-ABl lU&)=lp>|k^bl0-ToE1Sf@&B)<L}I9Zf');
define('SECURE_AUTH_KEY',  '5Z^wjR4?Dfx1(|48SR6t5!Ol3-7}b_p8zsuFhF;fx(rj[TF^k=uGJl_i}u$;P$ss');
define('LOGGED_IN_KEY',    '_<Plw:$$VL/HUTkL=Dt^M~o79$y-E01bW?()(|fQh/,NA/=qkFss@;lr%ZN&-.Sn');
define('NONCE_KEY',        '}@z-,ju-<u9+A*rg3ufyuW{licSxGor6IeF0xvB[R`rjB|M8%fb#Os@Z*W{ILkYH');
define('AUTH_SALT',        '+Z0;g^fvJ@KFF$OjJ8mrE>&#Ix&5^vD+XueLx^eEA7%v;g}lFuAo+;iTXOW2&Ddw');
define('SECURE_AUTH_SALT', 'fBj<KyU7HG0H+LX?RQ-S4aSv]F9cC_Z0j=IvS=$%F@} #*@h>tS/,<uG]@z0;g~M');
define('LOGGED_IN_SALT',   '}Lenk_b_5=z#G-k&~)AJ`(B4FlrO<]n%TIrUP(Dp<#^)8t[;Qr{*p ZPYp>cXZGi');
define('NONCE_SALT',       '9^/FhA<UjB)j,mi,B.=UkLnUM#?l]uXYf%nkbJ2K8lY%r%+@ g6:I4j`AT83QWn:');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
